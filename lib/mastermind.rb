require 'byebug'
class Code

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow,
  }

  attr_reader :pegs
  def self.parse(string)
    pegs = string.chars.map { |el| PEGS[el.upcase] }
    Code.new(pegs)
     if string.upcase.chars - PEGS.keys != []
       raise ArgumentError
     else
       self.new(pegs)
     end
  end

  def self.random
    pegs = PEGS.keys.sample(4)
    self.new(pegs)
  end

  def initialize (pegs)
    @pegs = pegs
  end

  def[](idx)
    @pegs[idx]
  end

  def exact_matches(other)
    count = 0
    @pegs.each_with_index do |peg,idx|
      if @pegs[idx] == other[idx]
        count+=1
      end
    end
    count
  end

  def near_matches(other)
   in1 = self.pegs
   in2 = other.pegs
   x = (in1 & in2).size - exact_matches(other)
   if x < 0
     x = 0
   end
 end

  def ==(other)
    return false unless other.class == Code
    @pegs == other.pegs
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil )
    @secret_code = secret_code || Code.random
    if @secret_code == nil
      @secret_code = PEGS.keys.sample(4)
    end
  end

  def play
    10.times do |turn|
        code = get_guess
        break if won?(code)
        display_matches(code)
      end
      if code == @secret_code
        puts "YOU WON!"
      else
      puts "Sorry you lost "
      puts "The answer was #{@secret_code}"
    end
  end

  def won?(code)
    @secret_code == code
  end

  def get_guess
    p "Please choose a move in the form of (BBBB)"
    input = gets.chomp!
    Code.parse(input)
  end

  def display_matches(code)
    puts "near matches #{@secret_code.near_matches(code)}"
    puts "exact matches #{@secret_code.exact_matches(code)}"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = Game.new(Code.parse('bbbb'))
  game.play
end
